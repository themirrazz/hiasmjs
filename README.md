# HiAsm.JS
HiAsm.JS is an open-source library for parsing HiAsm programs and HiAsm packs.

## Use in the browser
Download "hiasm.min.js" and upload it to your website. Then the HiAsm namespace will be availible.

## Use in an Embedded Program (eg Windows 96 app)
Copy the code in "hiasm-e.min.js" and paste the code into a variable.
Example:
```js
var hiasm = <paste code here>
```

## Use in NodeJS
Please be patient, we will publish to NPM soon!

## Open-Source Software Libraries
**SQL.JS** https://github.com/sql-js/sql.js Licensed under MIT License
